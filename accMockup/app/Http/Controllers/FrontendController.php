<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function dashboard()
    {
        return view('frontend.dashboard');
    }

    public function newInvoice()
    {
        return view('frontend.newInvoice');
    }

    public function newBill()
    {
        return view('frontend.newBill');
    }

    public function miscellaneousEntry()
    {
        return view('frontend.miscellaneousEntry');
    }

    public function chartOfAccounts()
    {
        return view('frontend.chartOfAccounts');
    }

    public function balanceSheet()
    {
        return view('frontend.balanceSheet');
    }

    public function settings()
    {
        return view('frontend.settings');
    }

    public function journalEntries()
    {
        return view('frontend.journalEntries');
    }
}
