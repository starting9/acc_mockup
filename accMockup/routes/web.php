<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\JournalController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', [FrontendController::class, 'dashboard'])->name('dashboard');
Route::get('/chartOfAccounts', [FrontendController::class, 'chartOfAccounts'])->name('chartOfAccounts');
Route::get('/newInvoice', [FrontendController::class, 'newInvoice'])->name('newInvoice');
Route::get('/newBill', [FrontendController::class, 'newBill'])->name('newBill');
Route::get('/balanceSheet', [FrontendController::class, 'balanceSheet'])->name('balanceSheet');
Route::get('/settings', [FrontendController::class, 'settings'])->name('settings');

Route::resource('journals', JournalController::class);
