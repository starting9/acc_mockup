<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Accounting</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="{{ route('dashboard') }}">Dashboard</a>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="customers-dd" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Customers
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="customers-dd">
                        <li><a class="dropdown-item" href="#">Invoice</a></li>
                        <li><a class="dropdown-item" href="#">Credit Notes</a></li>
                        <li><a class="dropdown-item" href="#">Payments</a></li>
                        <li><a class="dropdown-item" href="#">Follow-up Reports</a></li>
                        <li><a class="dropdown-item" href="#">Products</a></li>
                        <li><a class="dropdown-item" href="#">Customers</a></li>
                    </ul>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="vendors-dd" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Vendors
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="vendors-dd">
                        <li><a class="dropdown-item" href="#">Bills</a></li>
                        <li><a class="dropdown-item" href="#">Refunds</a></li>
                        <li><a class="dropdown-item" href="#">Payments</a></li>
                        <li><a class="dropdown-item" href="#">Products</a></li>
                        <li><a class="dropdown-item" href="#">Vendors</a></li>
                    </ul>
                </li>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="vendors-dd" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Accounting
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="vendors-dd">
                        <li><a class="dropdown-header" href="#">Miscellaneous</a></li>
                        <li><a class="dropdown-item" href="{{ route('journals.index') }}">Journal Entries</a></li>
                        
                        <li><a class="dropdown-header" href="#">Journals</a></li>
                        <li><a class="dropdown-item" href="#">Sales</a></li>
                        <li><a class="dropdown-item" href="#">Purchases</a></li>
                        <li><a class="dropdown-item" href="#">Bank & Cash</a></li>
                        <li><a class="dropdown-item" href="#">Miscellaneous</a></li>
                        
                        <li><a class="dropdown-header" href="#">Ledgers</a></li>
                        <li><a class="dropdown-item" href="#">General Ledger</a></li>
                        <li><a class="dropdown-item" href="#">Partner Ledger</a></li>

                        <li><a class="dropdown-header" href="#">Management</a></li>
                        <li><a class="dropdown-item" href="#">Automatic Transfers</a></li>
                        <li><a class="dropdown-item" href="#">Assets</a></li>
                        <li><a class="dropdown-item" href="#">Deferred Revenues</a></li>
                        <li><a class="dropdown-item" href="#">Deferred Expenses</a></li>

                        <li><a class="dropdown-header" href="#">Actions</a></li>
                        <li><a class="dropdown-item" href="#">Reconciliation</a></li>
                        <li><a class="dropdown-item" href="#">Lock Dates</a></li>
                    </ul>
                </li>



                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="vendors-dd" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Reporting
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="vendors-dd">
                        <li><a class="dropdown-header" href="#">Generic Statements</a></li>
                        <li><a class="dropdown-item" href="{{ route('balanceSheet') }}">Balance Sheet</a></li>
                        <li><a class="dropdown-item" href="#">Executive Summary</a></li>
                        <li><a class="dropdown-item" href="#">Profit and Loss</a></li>
                        <li><a class="dropdown-item" href="#">Cash Flow Statement</a></li>

                        <li><a class="dropdown-header" href="#">Partner Reports</a></li>
                        <li><a class="dropdown-item" href="#">Partner Ledger</a></li>
                        <li><a class="dropdown-item" href="#">Aged Receivable</a></li>
                        <li><a class="dropdown-item" href="#">Aged Payable</a></li>

                        <li><a class="dropdown-header" href="#">Audit Reports</a></li>
                        <li><a class="dropdown-item" href="#">General Ledger</a></li>
                        <li><a class="dropdown-item" href="#">Trial Balance</a></li>
                        <li><a class="dropdown-item" href="#">Consolidated Journals</a></li>
                        <li><a class="dropdown-item" href="#">Tax Report</a></li>
                        <li><a class="dropdown-item" href="#">Journals Audit</a></li>

                        <li><a class="dropdown-header" href="#">Management</a></li>
                        <li><a class="dropdown-item" href="#">Invoice Analysis</a></li>
                        <li><a class="dropdown-item" href="#">Unrealized Currency Gains/Losses</a></li>
                        <li><a class="dropdown-item" href="#">Depreciation Schedule</a></li>
                    </ul>
                </li>



                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="vendors-dd" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Configuration
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="vendors-dd">
                        <li><a class="dropdown-item" href="{{ route('settings') }}">Settings</a></li>

                        <li><a class="dropdown-header" href="#">Invoicing</a></li>
                        <li><a class="dropdown-item" href="#">Payment Terms</a></li>
                        <li><a class="dropdown-item" href="#">Follow-up Levels</a></li>
                        <li><a class="dropdown-item" href="#">Incoterms</a></li>

                        <li><a class="dropdown-header" href="#">Banks</a></li>
                        <li><a class="dropdown-item" href="#">Add a Bank Account</a></li>
                        <li><a class="dropdown-item" href="#">Reconciliation Models</a></li>
                        <li><a class="dropdown-item" href="#">Online Synchronization</a></li>

                        <li><a class="dropdown-header" href="#">Accounting</a></li>
                        <li><a class="dropdown-item" href="{{ route('chartOfAccounts') }}">Chart of Accounts</a></li>
                        <li><a class="dropdown-item" href="#">Taxes</a></li>
                        <li><a class="dropdown-item" href="#">Journals</a></li>
                        <li><a class="dropdown-item" href="#">Currencies</a></li>
                        <li><a class="dropdown-item" href="#">Fiscal Positions</a></li>
                        <li><a class="dropdown-item" href="#">Journal Groups</a></li>

                        <li><a class="dropdown-header" href="#">Payments</a></li>
                        <li><a class="dropdown-item" href="#">Payment Acquirers</a></li>

                        <li><a class="dropdown-header" href="#">Management</a></li>
                        <li><a class="dropdown-item" href="#">Asset Models</a></li>
                        <li><a class="dropdown-item" href="#">Deferred Revenue Models</a></li>
                        <li><a class="dropdown-item" href="#">Product Categories</a></li>
                        <li><a class="dropdown-item" href="#">Deferred Expense Models</a></li>
                    </ul>
                </li>
            </ul>
            <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>