<x-frontend.layouts.master>
    <h2>SETTINGS</h2>

    <div class="container">

        <h4><em>Taxes</em></h4>

        <div class="row">
            <div class="col-md-6">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('imgs/tax2.jpg') }}" class="card-img-top" alt="..." height="200px">
                    <div class="card-body">
                        <h5 class="card-title">Default Taxes</h5>
                        <div class="row">
                            <p class="card-text">Sales Tax</p>
                            <select class="form-select">
                                <option>Tax 15.00 %</option>
                                <option>Tax 10.00 %</option>
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <p class="card-text">Puchase Tax</p>
                            <select class="form-select">
                                <option>Tax 15.00 %</option>
                                <option>Tax 10.00 %</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('imgs/tax.jpg') }}" class="card-img-top" alt="..." height="200px">
                    <div class="card-body">
                        <h5 class="card-title">Tax Return Periodicity</h5>
                        <div class="row">
                            <p class="card-text">Periodicity</p>
                            <select class="form-select">
                                <option>Monthly</option>
                                <option>Yearly</option>
                            </select>
                        </div>
                        <br>
                        <div class="row">
                            <p class="card-text">Reminder</p>
                            <select class="form-select">
                                <option>3 Days</option>
                                <option>7 Days</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br><br>

        <h4><em>Currency</em></h4>
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('imgs/money.jpg') }}" class="card-img-top" alt="..." height="200px">
                    <div class="card-body">
                        <h5 class="card-title">Main Currency</h5>
                        <div class="row">
                            <p class="card-text">Currency</p>
                            <select class="form-select">
                                <option>USD</option>
                                <option>BDT</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br><br><br><br>

        <h4><em>Fiscal Periods</em></h4>
        <div class="row">
            <div class="col-md-6">
                <div class="card" style="width: 18rem;">
                    <img src="{{ asset('imgs/money.jpg') }}" class="card-img-top" alt="..." height="200px">
                    <div class="card-body">
                        <h5 class="card-title">Fiscal Year</h5>
                        <div class="row">
                            <p class="card-text">Last Day</p>
                            <div class="col-md-6">
                                <select class="form-select">
                                    <option>Jun</option>
                                    <option>Dec</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select class="form-select">
                                    <option>31</option>
                                    <option>1</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="card-text">Fiscal Country</p>
                                    <select class="form-select">
                                        <option>Bangladesh</option>
                                        <option>India</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</x-frontend.layouts.master>