<x-frontend.layouts.master>
    <h2>Accounting Dashboard</h2>
    <div class="container">
        <div class="row row-cols-1 row-cols-md-2 g-4">
            <div class="col">
                <div class="card">
                    <img src="{{ asset('imgs/graphs.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Customer Invoices</h5>
                        <p class="card-text">Invoices to Validate : 9</p>
                        <a href="{{ route('newInvoice') }}">
                            <button type="button" class="btn btn-secondary">New Invoice</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="{{ asset('imgs/graphs.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Vendor Bills</h5>
                        <p class="card-text">Late Bills : 10 | Bills to Pay : 7</p>
                        <a href="{{ route('newBill') }}">
                            <button type="button" class="btn btn-secondary">New Bill</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="{{ asset('imgs/graphs.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Miscellaneous Operations</h5>
                        <p class="card-text">Tax return for August.</p>
                        <a href="{{ route('journals.create') }}">
                            <button type="button" class="btn btn-secondary">New Entry</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="{{ asset('imgs/graphs.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Cash</h5>
                        <p class="card-text">Outstanding Payments/Receipts : $580</p>
                        <button type="button" class="btn btn-secondary">New Transaction</button>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="card">
                    <img src="{{ asset('imgs/graphs.jpg') }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Bank</h5>
                        <button type="button" class="btn btn-secondary">Online Synchronization</button>
                        <button type="button" class="btn btn-secondary">Create / Import Statements</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-frontend.layouts.master>