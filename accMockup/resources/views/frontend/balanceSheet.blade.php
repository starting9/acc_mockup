<x-frontend.layouts.master>
    <h2>Balance Sheet</h2>
    <div style="display: flex; justify-content:space-around;">
        <h3><button type="button" class="btn btn-secondary">PDF</button></h3>
        <h3><button type="button" class="btn btn-secondary">XLSX</button></h3>
        <h3><button type="button" class="btn btn-secondary">SAVE</button></h3>
    </div>
    <table class="table">
        <thead>
            <th scope="col">ASSETS</th>
            <th></th>
        </thead>
        <tbody>
            <tr>
                <td>Current Assets</td>
                <td></td>
            </tr>
            <tr>
                <td>Bank and Cash Accounts</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Receivables</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Current Assets</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Prepayments</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total Current Assets</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Plus Fixed Assets</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Plus Non-Current Assets</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total ASSETS</td>
                <td></td>
            </tr>
        </tbody>
    

        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
    
        <thead>
            <tr>
                <th scope="col">LIABILITIES</td>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Current Liablities</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Payables</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total Current Liablities</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Plus Non-Current Liabilities</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total LIABILITIES</td>
                <td>$0.00</td>
            </tr>
        </tbody>
    

        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>

    
        <thead>
            <tr>
                <th scope="col">EQUITY</td>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Current Year Unallocated Earnings</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Previous Years Unallocated Earnings</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total Unallocated Earnings</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Retained Earnings</td>
                <td>$0.00</td>
            </tr>
            <tr>
                <td>Total EQUITY</td>
                <td>$0.00</td>
            </tr>
        </tbody>
    
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr></tr>

    
        <thead>
            <th scope="col">LIABILITIES + EQUITY</th>
            <th scope="col">$0.00</th>
        </thead>
    </table>
</x-frontend.layouts.master>