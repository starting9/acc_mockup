<x-frontend.layouts.master>
    <!-- <p>Date / Number / Partner / Reference / Journal / Total / Status</p>
    <p>d.m.y / MISC.no. / Blank / Blank / Type / Amount / Draft/Posted</p> -->



    <table class="table">
        <thead>
            <th scope="col">Journal</th>
            <th scope="col">Reference</th>
            <th scope="col">Date</th>
            <th scope="col">Status</th>
        </thead>
        <tbody>
            @foreach($journals as $journal)
            <tr>
            <td>{{ $journal->journalNo }}</td>
            <td>{{ $journal->reference }}</td>
            <td>{{ $journal->acc_date }}</td>
            <td>Drafted / Posted</td>
            </tr>
            @endforeach
        </tbody>
    </table>

</x-frontend.layouts.master>