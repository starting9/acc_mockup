<x-frontend.layouts.master>
    <h2>Miscellaneous Entry</h2>
    <div style="display: flex;">
        <button type="button" class="btn btn-primary" style="margin-right: 5px;">Save</button>
        <button type="button" class="btn btn-secondary" style="margin-right: 5px;">Discard</button>
        <button type="button" class="btn btn-warning" style="margin-right: 5px;">Post</button>
    </div>
    <div class="container">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach 
        </ul>
    </div>
    @endif

        <form action="{{ route('journals.store') }}" method="post">
            @csrf
            <br>
            <br><br>
            <div class="row">
                <div class="col">
                    <label>Journal Number</label>
                    <input name="journalNo" type="text" class="form-control" placeholder="MISC/123456" value="{{ old('journalNo') }}">
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="customer" class="form-label">Reference</label>
                    <input name="reference" type="text" class="form-control" aria-label="customer" value="{{ old('reference') }}">
                </div>
                <div class="col">
                    <label for="invoiceDate" class="form-label">Accounting Date</label>
                    <input name="acc_date" type="date" placeholder="dd-mm-yyyy" class="form-control" aria-label="invoiceDate" value="{{ old('acc_date') }}">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col"></div>

                <div class="col">
                    <label for="dueDate" class="form-label">Journal</label>
                    <select class="form-select">
                        <option>Miscellaneous Operations</option>
                        <option>Exchange Difference</option>
                        <option>Cash Basis Taxes</option>
                    </select>
                </div>
            </div>
            <br>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Invoice Lines</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Journal Items</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Other Info</button>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <table class="table">
                        <thead>
                            <th scope="col">Product</th>
                            <th scope="col">Label</th>
                            <th scope="col">Account</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                            <th scope="col">Taxes</th>
                            <th scope="col">SubTotal</th>
                        </thead>
                        <tbody>
                            <td>Add a Line</td>
                            <td>Add a Section</td>
                            <td>Add a Note</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <table class="table">
                        <thead>
                            <th scope="col">Account</th>
                            <th scope="col">Label</th>
                            <th scope="col">Debit</th>
                            <th scope="col">Credit</th>
                            <th scope="col">Tax Grids</th>
                        </thead>
                        <tbody>
                            <td>Add a Line</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                            <td>...</td>
                        </tbody>
                    </table>
                </div>

                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>


</x-frontend.layouts.master>