<x-frontend.layouts.master>
    <h2>Chart Of Accounts</h2>
    <div class="container">
    <button type="button" class="btn btn-secondary">Create</button>
    <button type="button" class="btn btn-secondary">Export</button>
    </div>
    <table class="table">
        <thead>
            <th scope="col">Code</th>
            <th scope="col">Account Name</th>
            <th scope="col">Type</th>
            <th scope="col">Allow Reconciliation</th>
            <th scope="col">Account Currency</th>
            <th scope="col">
                <select>
                    <option>Group</option>
                    <option>Default Taxes</option>
                    <option>Tags</option>
                    <option>Allowed Journals</option>
                </select>
            </th>
        </thead>
        <tbody>
            <tr>
                <td>100000</td>
                <td>Asset</td>
                <td>??</td>
                <td>??</td>
                <td>
                    <select>
                        <option>BDT</option>
                        <option>USD</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-secondary">SETUP</button></td>
            </tr>

            <tr>
                <td>200000</td>
                <td>Liabilities</td>
                <td>??</td>
                <td>??</td>
                <td>
                    <select>
                        <option>BDT</option>
                        <option>USD</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-secondary">SETUP</button></td>
            </tr>

            <tr>
                <td>300000</td>
                <td>Equity</td>
                <td>??</td>
                <td>??</td>
                <td>
                    <select>
                        <option>BDT</option>
                        <option>USD</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-secondary">SETUP</button></td>
            </tr>

            <tr>
                <td>400000</td>
                <td>Income</td>
                <td>??</td>
                <td>??</td>
                <td>
                    <select>
                        <option>BDT</option>
                        <option>USD</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-secondary">SETUP</button></td>
            </tr>

            <tr>
                <td>300000</td>
                <td>Expense</td>
                <td>??</td>
                <td>??</td>
                <td>
                    <select>
                        <option>BDT</option>
                        <option>USD</option>
                    </select>
                </td>
                <td><button type="button" class="btn btn-secondary">SETUP</button></td>
            </tr>
        </tbody>
    </table>
</x-frontend.layouts.master>