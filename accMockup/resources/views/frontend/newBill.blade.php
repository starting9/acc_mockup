<x-frontend.layouts.master>
    <h2>Vendor Bill Draft</h2>
    <div class="container">
        <br>
        <div style="display: flex; justify-content:space-around">
            <button type="button" class="btn btn-secondary">Preview</button>
            <button type="button" class="btn btn-secondary">Confirm</button>
        </div>
        <br><br>
        <div class="row">
            <div class="col">
                <label for="customer" class="form-label">Vendor</label>
                <input type="text" class="form-control" aria-label="customer">
            </div>
            <div class="col">
                <label for="invoiceDate" class="form-label">Bill Date</label>
                <input type="date" class="form-control" aria-label="invoiceDate">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <label for="paymentReference" class="form-label">Bill Reference</label>
                <input type="text" class="form-control" aria-label="paymentReference">
            </div>
            <div class="col">
                <label for="dueDate" class="form-label">Accounting Date</label>
                <input type="date" class="form-control" aria-label="dueDate">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <label for="paymentReference" class="form-label">Payment Reference</label>
                <input type="text" class="form-control" aria-label="paymentReference">
            </div>
            <div class="col">
                <label for="dueDate" class="form-label">Due Date</label>
                <input type="date" class="form-control" aria-label="dueDate">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col">
                <label for="paymentReference" class="form-label">Recipient Bank</label>
                <input type="text" class="form-control" aria-label="paymentReference">
            </div>
            <div class="col">
                <label for="dueDate" class="form-label">Journal</label>
                <div class="row">
                    <div class="col-md-6">
                        <input type="text" class="form-control" aria-label="dueDate" placeholder="Vendor Bill">
                    </div>
                    <div class="col-md-1">
                        <label>in</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" aria-label="" placeholder="USD">
                    </div>
                </div>
            </div>
        </div>
        <br>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
                <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Invoice Lines</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Journal Items</button>
            </li>
            <li class="nav-item" role="presentation">
                <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Other Info</button>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <table class="table">
                    <thead>
                        <th scope="col">Product</th>
                        <th scope="col">Label</th>
                        <th scope="col">Account</th>
                        <th scope="col">Quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Taxes</th>
                        <th scope="col">SubTotal</th>
                    </thead>
                    <tbody>
                        <td>Add a Line</td>
                        <td>Add a Section</td>
                        <td>Add a Note</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <table class="table">
                    <thead>
                        <th scope="col">Account</th>
                        <th scope="col">Label</th>
                        <th scope="col">Debit</th>
                        <th scope="col">Credit</th>
                        <th scope="col">Tax Grids</th>
                    </thead>
                    <tbody>
                        <td>Add a Line</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                        <td>...</td>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
        </div>


</x-frontend.layouts.master>